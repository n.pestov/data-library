﻿using System;
using System.Runtime.InteropServices;

namespace Null.DataLibrary.ComplexFlags.Abstract
{
    /// <summary>
    /// Base realization <see cref="IEquatable{T}"/> in complex flags.
    /// </summary>
    /// <typeparam name="T">BaseComplexFlags</typeparam>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public abstract class BaseComplexFlagsEquatable<T> : BaseComplexFlags, IEquatable<T>
        where T : BaseComplexFlags
    {
        /// <inheritdoc/>
        public override bool Equals(object other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            return GetType() == other.GetType() && Equals((T)other);
        }

        /// <inheritdoc/>
        public bool Equals(T other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            return GetType() == other.GetType() &&
            Value.Equals(other.Value);
        }

        /// <inheritdoc/>
        public override int GetHashCode()
            => Value.GetHashCode();
    }
}