﻿using System.Runtime.InteropServices;

namespace Null.DataLibrary.ComplexFlags.Abstract
{
    /// <summary>
    /// Base complex flags class.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public abstract class BaseComplexFlags
    {
        private byte _value;
        /// <summary>
        /// Возвращает или задаёт итоговое значение всех полей.
        /// </summary>
        public byte Value
        {
            get => _value;
            set => _value = value;
        }
    }
}