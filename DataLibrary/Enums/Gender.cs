﻿namespace Null.DataLibrary.Enums
{
    /// <summary>
    /// Пол.
    /// </summary>
    public enum Gender : byte
    {
        Unknown,
        Male,
        Female
    }
}