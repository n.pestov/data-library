﻿namespace Null.DataLibrary.Enums
{
    /// <summary>
    /// Наименования типов записи для работы с анализаторами данных ЭКГ
    /// </summary>
    public static class RecordNameType
    {
        public const string TwoLeadECG = "Холтер 2 кан.ЭКГ";
        public const string ThreeLeadECG = "Холтер 3 кан.ЭКГ";
        public const string ThreeLeadECGHighRes = "Холтер 3 кан.ЭКГ Выс.разр.";
        public const string ThirtyTwoLeadECG = "Холтер 32 кан.ЭКГ";
        public const string TwoLeadECGHighRes = "Холтер 2 кан.ЭКГ Выс.разр.";
        public const string TwelveLeadECG = "Холтер 12 кан. ЭКГ";
        public const string TwoLeadECGPB = "Холтер 2 кан.ЭКГ + АД";
        public const string ThreeLeadECGPB = "Холтер 3 кан.ЭКГ + АД";
        public const string ThreeLeadECGActive = "Холтер 3 кан.ЭКГ + Актив.";
        public const string TwelveLeadECGPB = "Холтер 12 кан.ЭКГ + АД";
    }
}