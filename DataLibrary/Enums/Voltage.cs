﻿namespace Null.DataLibrary.Enums
{
    /// <summary>
    /// Вольтаж.
    /// </summary>
    public enum Voltage : byte
    {
        Standard,
        Low,
        High,
        Auto
    }
}