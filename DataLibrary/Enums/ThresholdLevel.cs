﻿namespace Null.DataLibrary.Enums
{
    /// <summary>
    /// Чувствительность детекции стимула.
    /// </summary>
    public enum ThresholdLevel : byte
    {
        Standard,
        High,
        Max,
        Min
    }
}