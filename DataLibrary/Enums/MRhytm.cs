﻿namespace Null.DataLibrary.Enums
{
    public enum MRhytm : byte
    {
        SinusRhytm,
        AtrialFibr,
        PesmRhytm,
        AV_BLOCK_III,
        AV_BLOCK_III_AF
    }
}