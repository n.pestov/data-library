﻿namespace Null.DataLibrary.Enums
{
    /// <summary>
    /// Индекс отведения для первого канала 12 канальной записи.
    /// </summary>
    public enum AnalCh12 : byte
    {
        V6,
        V5,
        V4,
        V3,
        V2,
        V1,
        AVF,
        AVL,
        AVR,
        III,
        II,
        I
    }
}