﻿namespace Null.DataLibrary.Enums
{
    /// <summary>
    /// Значение типа записи.
    /// </summary>
    public enum RecordType : byte
    {
        Holter2Channel_12 = 12,
        Holter2Channel_13 = 13,
        Holter3Channel = 14,
        Holter3ChannelHighResolution = 15,
        Holter2ChannelHighResolution = 16,
        Holter12Channel = 17,
        Holter2Channel_BloodPressure = 18,
        Holter3Channel_BloodPressure = 20,
        HolterBloodPressure = 21,
        Holter2Channel_22 = 22,
        Holter3Channel_ActivitySensor = 23,
        Holter12Channel_BloodPressure = 25,
        Holter32Channel = 26
    }
}