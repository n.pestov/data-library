﻿namespace Null.DataLibrary.Enums
{
    /// <summary>
    /// Разрешающая способность.
    /// </summary>
    public enum Resolution : byte
    {
        VeryHigh,
        High,
        Middle,
        Low,
        Auto
    }
}