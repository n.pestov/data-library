﻿namespace Null.DataLibrary.Enums
{
    /// <summary>
    /// Состояние анализа искусственного водителя ритма.
    /// </summary>
    public enum PsmStyle : byte
    {
        Off,
        On
    }
}