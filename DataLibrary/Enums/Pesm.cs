﻿namespace Null.DataLibrary.Enums
{
    /// <summary>
    /// Значение, означающие поддержку детекции стимула регистратором.
    /// </summary>
    public enum Pesm : byte
    {
        No,
        Yes
    }
}