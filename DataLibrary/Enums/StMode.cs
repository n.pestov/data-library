﻿namespace Null.DataLibrary.Enums
{
    /// <summary>
    /// Тип анализа ST.
    /// </summary>
    public enum StMode : byte
    {
        Standard = 0b00,
        Adapt = 0b10
    }
}