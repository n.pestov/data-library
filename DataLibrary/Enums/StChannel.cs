﻿namespace Null.DataLibrary.Enums
{
    /// <summary>
    /// Канал для анализа.
    /// </summary>
    public enum StChannel : byte
    {
        First = 1,
        Second,
        Third
    }
}