﻿namespace Null.DataLibrary.Enums
{
    /// <summary>
    /// Индекс отведения для определенного канала. Эти данные являются исключительно данными отображения пользователю.
    /// </summary>
    public enum Lead : byte
    {
        NotConnected,
        LeftThoracic,
        RightThoracic,
        LeftThoracic_V2,
        V1,
        V2,
        V3,
        V4,
        V5,
        V6,
        X,
        Y,
        Z,
        Stimulator,
        Dorsalis,
        Anterior,
        Interior,
        I,
        II,
        III
    }
}