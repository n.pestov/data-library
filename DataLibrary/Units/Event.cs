﻿using System;
using System.Runtime.InteropServices;

namespace Null.DataLibrary.Units
{
    /// <summary>
    /// Patient event.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Event : IEquatable<Event>
    {
        private uint _start;
        private uint _end;

        /// <summary>
        /// Время начала события.
        /// </summary>
        public uint Start
        {
            get => _start;
            set => _start = value;
        }

        /// <summary>
        /// Время окончания события.
        /// </summary>
        public uint End
        {
            get => _end;
            set => _end = value;
        }

        /// <inheritdoc/>
        public override bool Equals(object other)
        {
            return other != null
                && GetType() == other.GetType() && Equals((Event)other);
        }

        /// <inheritdoc/>
        public bool Equals(Event other)
            => GetType() == other.GetType() &&
            _start.Equals(other._start) &&
            _end.Equals(other._end);

        /// <inheritdoc/>
        public override int GetHashCode() => (_start, _end).GetHashCode();

        public static bool operator ==(Event left, Event right) => left.Equals(right);

        public static bool operator !=(Event left, Event right) => !(left == right);
    }
}