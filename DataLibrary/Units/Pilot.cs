﻿using System;
using System.Runtime.InteropServices;

namespace Null.DataLibrary.Units
{
    /// <summary>
    /// Data needed to search blocks in record file.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Pilot : IEquatable<Pilot>
    {
        private int _recordFileNum;
        private uint _blockPosition;

        /// <summary>
        /// Номер файла записи.
        /// </summary>
        public int RecordFileNum
        {
            get => _recordFileNum;
            set => _recordFileNum = value;
        }

        /// <summary>
        /// Позиция блока.
        /// </summary>
        public uint HardPosition
        {
            get => _blockPosition;
            set => _blockPosition = value;
        }

        /// <inheritdoc/>
        public override bool Equals(object other)
        {
            return other != null
                && GetType() == other.GetType() && Equals((Pilot)other);
        }

        /// <inheritdoc/>
        public bool Equals(Pilot other)
            => GetType() == other.GetType() &&
            _recordFileNum.Equals(other._recordFileNum) &&
            _blockPosition.Equals(other._blockPosition);

        /// <inheritdoc/>
        public override int GetHashCode() => (_recordFileNum, _blockPosition).GetHashCode();

        public static bool operator ==(Pilot left, Pilot right) => left.Equals(right);

        public static bool operator !=(Pilot left, Pilot right) => !(left == right);
    }
}