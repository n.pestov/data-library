﻿namespace Null.DataLibrary
{
    /// <summary>
    /// Generic ICloneable.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal interface ICloneable<T>
    {
        /// <summary>
        /// A method for cloning.
        /// </summary>
        /// <returns></returns>
        T Clone();
    }
}