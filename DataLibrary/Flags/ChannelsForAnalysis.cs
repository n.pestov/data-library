﻿using System;

namespace Null.DataLibrary.Flags
{
    /// <summary>
    /// Каналы для анализа.
    /// </summary>
    [Flags]
    public enum ChannelsForAnalysis : byte
    {
        None,
        First = 0b0001,
        Second = 0b0010,
        Third = 0b0100,
        Auto = 0b1000
    }
}