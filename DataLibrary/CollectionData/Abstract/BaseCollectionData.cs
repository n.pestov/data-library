﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Null.DataLibrary.CollectionData.Abstract
{
    /// <summary>
    /// The base data collection of units.
    /// </summary>
    /// <typeparam name="T">Data</typeparam>
    /// <typeparam name="TU">Unit</typeparam>
    public abstract class BaseCollectionData<T, TU> : ICollection<TU>, IEnumerable<TU>
        where TU : IEquatable<TU>
    {
        /// <summary>
        /// Unit of data collection.
        /// </summary>
        protected ICollection<TU> _units;

        /// <inheritdoc/>
        public int Count => _units.Count;

        /// <inheritdoc/>
        public bool IsReadOnly => _units.IsReadOnly;

        /// <summary>
        /// BaseCollectionData ctor.
        /// </summary>
        public BaseCollectionData()
        {
        }

        /// <inheritdoc/>
        public IEnumerator<TU> GetEnumerator() => _units.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        /// <inheritdoc/>
        public void Add(TU item) => _units.Add(item);

        /// <inheritdoc/>
        public void Clear() => _units.Clear();

        /// <inheritdoc/>
        public bool Contains(TU item) => _units.Contains(item);

        /// <inheritdoc/>
        public void CopyTo(TU[] array, int arrayIndex) => _units.CopyTo(array, arrayIndex);

        /// <inheritdoc/>
        public bool Remove(TU item) => _units.Remove(item);
    }
}