﻿using System.Collections.Generic;
using Null.DataLibrary.CollectionData.Abstract;
using Null.DataLibrary.Units;

namespace Null.DataLibrary.CollectionData.Realization
{
    /// <summary>
    /// Collection of pilots.
    /// </summary>
    public sealed class PilotData : BaseCollectionData<PilotData, Pilot>, ICloneable<PilotData>
    {
        /// <summary>
        /// PilotData ctor.
        /// </summary>
        public PilotData() : base()
        {
            _units = new HashSet<Pilot>
            {
                new Pilot()
            };
        }

        /// <inheritdoc/>
        public PilotData Clone()
        {
            var pilotData = new PilotData();

            foreach (var pilot in _units)
            {
                pilotData.Add(pilot);
            }

            return pilotData;
        }
    }
}