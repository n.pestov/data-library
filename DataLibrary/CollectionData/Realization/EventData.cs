﻿using System.Collections.Generic;
using Null.DataLibrary.CollectionData.Abstract;
using Null.DataLibrary.Units;

namespace Null.DataLibrary.CollectionData.Realization
{
    /// <summary>
    /// Collection of events.
    /// </summary>
    public sealed class EventData : BaseCollectionData<EventData, Event>, ICloneable<EventData>
    {
        /// <summary>
        /// EventData ctor.
        /// </summary>
        public EventData() : base()
        {
            _units = new HashSet<Event>();
        }

        /// <inheritdoc/>
        public EventData Clone()
        {
            var eventData = new EventData();

            foreach (var eventUnit in _units)
            {
                eventData.Add(eventUnit);
            }

            return eventData;
        }
    }
}